#define ARRAY_SIZE(array) ((sizeof(array)/sizeof(int))/2)
#include <jni.h>
#include <stdio.h>
#include <iostream>
#include <fstream>
#include <cv.h>
#include "opencv2/core/core.hpp"
#include "opencv2/features2d/features2d.hpp"
#include "opencv2/highgui/highgui.hpp"

using namespace cv;
using namespace std;

int constel[12][2]={
	//pictor
	{167, 174},
	{155, 102},
	{124, 176},
	//capricorn
	{161, 115},
	{327, 126},
	{233, 138},
	{241, 215},
	{317, 141},
	{218, 195},
	{190, 171},
	{186, 163},
	{165, 138}
};

//Color check
struct pointcolor{
    float rgb, x, y;
};

bool cmpc(pointcolor p, pointcolor q){
	return p.rgb > q.rgb;
}

//Search
vector<int> R01; // points which are R0 and R1
int best = 0; //current best
//calculating distance between 2 points
double dist(double x1, double y1, double x2, double y2)
{
	return sqrt((x1 - x2) * (x1 - x2) + (y1 - y2) * (y1 - y2));
}
double Sdist[1024][1024]; //distance from all points of the image to all the other 
//structure with distance and where to dist so we can sort it later
struct pdist
{
	int p;
	double dis;
	pdist () {}
	pdist (int _p, double _dis)
	{
		p = _p;
		dis = _dis;
	}
};
vector<pdist> distlist[1024]; //vector where we will keep the sorted distance from i to all the other points
//function that helps us sort
bool cmp(pdist x, pdist y)
{
	return x.dis < y.dis;
}
int used[1024]; //whether or not we have used the point for the current points we check for

const double eps = 1e-8; //allowed error

const char* findConstAlg(const char* image){
	IplImage* img = NULL;
	if ((img = cvLoadImage(image))== 0){
//		stringstream temp_str;
//		temp_str<<(best);
//		std::string str = temp_str.str();
//		const char* strReturn = 0;
//		return strReturn;
	}
	
	IplImage* gray_img = cvCreateImage(cvGetSize(img), IPL_DEPTH_8U, 1);
	CvMemStorage* storage = cvCreateMemStorage(0);
	
	//RGB to GrayScale
	cvCvtColor(img, gray_img, CV_BGR2GRAY);

	// This is done so as to prevent a lot of false points from being detected
	cvSmooth(gray_img, gray_img, CV_GAUSSIAN, 1, 1);

	IplImage* new_img_1 = cvCreateImage(cvGetSize(img),IPL_DEPTH_8U,1);
	cvCanny(gray_img, new_img_1, 75, 100, 3);
	
	// Detect the keypoints using Star Detector
	int minHessian = 20;

	StarFeatureDetector detector( minHessian );

	std::vector<KeyPoint> keypoints_1;
	
	detector.detect( new_img_1, keypoints_1 );

	//Sort by color
	int coords[keypoints_1.size()][2];
	for(int i=0;i<keypoints_1.size();i++){
		coords[i][0]=keypoints_1[i].pt.x;
		coords[i][1]=keypoints_1[i].pt.y;
	}
	int posc=0;
	pointcolor imgpoints[ARRAY_SIZE(coords)*3];
	for(int i=0;i<ARRAY_SIZE(coords);i++){
		for(int j=-1;j<=1;j++){
			//Get coordinates
			int x=coords[i][0]+j;
			int y=coords[i][1]+j;
			//Get color by coordinates
			int b = ((uchar *)(img->imageData + y*img->widthStep))[x*img->nChannels + 0];
			int g = ((uchar *)(img->imageData + y*img->widthStep))[x*img->nChannels + 1];
			int r = ((uchar *)(img->imageData + y*img->widthStep))[x*img->nChannels + 2];
			int rgb=r+g+b;
			if(rgb>=500){
				imgpoints[posc].rgb=rgb;
				imgpoints[posc].x=x;
				imgpoints[posc].y=y;
				posc++;
			}
		}
	}
	sort(imgpoints, imgpoints + posc, cmpc);
	cout<<posc<<endl;

		//-- Show detected (drawn) keypoints
	cvReleaseImage(&img);
	cvReleaseImage(&gray_img);
	cvReleaseImage(&new_img_1);

	//Search
	int i, j, k, c, pos;
	const int imgpoint = posc, con = 5; // number of points
	for(i = 0; i < con; i++) //points we check for
		R01.push_back(i);
	for(i = 0; i < imgpoint; i++) //calculating distance from all points of the image to all the others
	{
		for(j = 0; j < imgpoint; j++)
			if(i == j)
				continue;
			else
			{
				Sdist[i][j] = Sdist[j][i] = dist(imgpoints[i].x, imgpoints[i].y, imgpoints[j].x, imgpoints[j].y);
				distlist[i].push_back(pdist(j, Sdist[i][j]));
			}
			sort(distlist[i].begin(), distlist[i].end(), cmp);
	}
	for(i = 0; i < R01.size(); i++) //R0
		for(j = i + 1; j < R01.size(); j++) //R1
		{
			//if(i !=0 || j!=1)continue;
			double dr = dist(constel[R01[i]][0], constel[R01[i]][1], constel[R01[j]][0], constel[R01[j]][1]); //distance from R0 to R1
			for(k = 0; k < imgpoint; k++) //S0
				for(c = 0; c < imgpoint; c++) //S1
				{
					if(c == k) continue;
					double ds = dist(imgpoints[k].x, imgpoints[k].y, imgpoints[c].x, imgpoints[c].y); //distance from S0 to S1
					double t = dr / ds; //scaling
					int br = 2; //match count
					for(pos = 0; pos < con; pos++) //iterating through the stars of the constellation
					{
						if(pos == i || pos == j)
							continue ;
						double tmp, tmp2;
						tmp = dist(constel[R01[i]][0], constel[R01[i]][1], constel[pos][0], constel[pos][1]); //dist R0 - Rpos
						tmp /= t; //scaling the distance as it would be in the picture
						tmp2 = dist(constel[R01[j]][0], constel[R01[j]][1], constel[pos][0], constel[pos][1]); //dist R1 - Rpos
						tmp2 /= t;
						int l = -1, r = distlist[k].size();
						//we do binary search in order to find if the distance we seek present
						while(r - l > 1)
						{
							int mid = (l + r) / 2;
							if(tmp <= distlist[k][mid].dis)
								r = mid;
							else
								l = mid;
						}
						//after the binary search at position l we have the last element
						//which is not greater than what we search
						int tl = l, lamp = 0;
						//we go to the left to check for points we can use since there might be small differences
						//in the distance and there can be a few points at the same distance from our point
						//in the following while we check if the point was previously used
						//if the distance to S0 and S1 is right
						while(tl > 0)
						{
							if(fabs(distlist[k][tl].dis - tmp) > eps) break;
							if(used[distlist[k][tl].p] != (k * imgpoint + c) && fabs(Sdist[c][distlist[k][tl].p] - tmp2) < eps)
							{
								used[distlist[k][tl].p] = k * imgpoint + c;
								lamp = 1;
								break;
								br++;
							}
							tl--;
						}
						l++;
						//the same but to the right
						if(lamp == 0)
						while(l < distlist[k].size())
						{
							if(fabs(distlist[k][l].dis - tmp) > eps) break;
							if(used[distlist[k][l].p] != (k * imgpoint + c) && fabs(Sdist[c][distlist[k][l].p] - tmp2) < eps)
							{
								used[distlist[k][l].p] = k * imgpoint + c;
								lamp = 1;
								br++;
								break;
							}
							l++;
						}
					}
					if(br > best){
						best = br;
						if(br==2){
							cout<<imgpoints[k].x<<" "<<imgpoints[k].y<<endl;
						}
					}
				}
		}
	if(best < 1){
		best+=1;
	}
	stringstream temp_str;
	temp_str<<(best);
	std::string str = temp_str.str();
	const char* strReturn = str.c_str();
	return strReturn;
}

extern "C" {

JNIEXPORT jstring JNICALL Java_eu_techr_skykey_skykey_FindConstellation_findConst(JNIEnv* env, jobject, jstring image) {

	const jbyte* argvv = (jbyte*)env->GetStringUTFChars(image, NULL);

	char* argv =(char *) argvv;

    return env->NewStringUTF(findConstAlg(argv));

}

}