LOCAL_PATH := $(call my-dir)
 
include $(CLEAR_VARS)

OPENCV_LIB_TYPE:=STATIC
OPENCV_INSTALL_MODULES:=on
 
include D:/OpenCV-2.4.9-android-sdk/sdk/native/jni//OpenCV.mk
 
LOCAL_MODULE    := jni_part
LOCAL_SRC_FILES := jni_part.cpp
LOCAL_LDLIBS +=  -llog -ldl
 
include $(BUILD_SHARED_LIBRARY)