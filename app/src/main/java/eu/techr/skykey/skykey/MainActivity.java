package eu.techr.skykey.skykey;

import android.app.ActionBar;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.SignInButton;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;

public class MainActivity extends Activity {

    private LayoutInflater inflater;
    private static DrawerLayout mDrawerLayout;
    private static LinearLayout mDrawerLeft;
    private static LinearLayout mDrawerRight;
    private ActionBarDrawerToggle mDrawerToggle;
    public static ArrayList<String> appActivities = new ArrayList<>();
    public Activity activity;
    private SetButtonsActions buttonsActions;
    private Context context;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        setupActionBar();
        activity = this;
        context = getApplicationContext();

        /**
         * Setup Drawers
         */

        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        mDrawerLeft = (LinearLayout) findViewById(R.id.left_drawer);
        mDrawerRight = (LinearLayout) findViewById(R.id.right_drawer);

        // set a custom shadow that overlays the main content when the drawer opens
        mDrawerLayout.setDrawerShadow(R.drawable.shadow, GravityCompat.START);
        setDrawersLayouts();

        // ActionBarDrawerToggle ties together the the proper interactions
        // between the sliding drawer and the action bar app icon
        mDrawerToggle = new ActionBarDrawerToggle(
                this,                  /* host Activity */
                mDrawerLayout,         /* DrawerLayout object */
                R.string.drawer_open,  /* "open drawer" description for accessibility */
                R.string.drawer_close  /* "close drawer" description for accessibility */
        ) {
            public void onDrawerClosed(View v) {

            }

            public void onDrawerOpened(View v) {

            }
        };
        mDrawerLayout.setDrawerListener(mDrawerToggle);

        /**
         * Setup default layout
         */
        buttonsActions = new SetButtonsActions(context, activity, inflater);
        if (savedInstanceState == null) {
            LinearLayout handler = (LinearLayout) findViewById(R.id.content_frame);
            ViewGroup viewDefault = (ViewGroup) inflater.inflate(R.layout.home, handler, false);

            handler.addView(viewDefault);
            appActivities.add("home");
            buttonsActions.setOnClickListeners(new String[]{"home"});
            buttonsActions.seekBar();
        }

        /**
         * Setup default menus
         */

        String[] menus = {"left_drawer_content", "right_drawer_content", "action_barLayout"};
        buttonsActions.setOnClickListeners(menus);
        updateUI(false, "");
    }

    @Override
    public void onBackPressed() {
        LinearLayout handler = (LinearLayout) findViewById(R.id.content_frame);
        if (appActivities.size() - 2 >= 0) {
            ViewGroup viewNew = (ViewGroup) inflater.inflate(context.getResources().getIdentifier(appActivities.get(appActivities.size() - 2),
                    "layout", context.getPackageName()), handler, false);
            ViewGroup view = (ViewGroup) findViewById(getResources().getIdentifier(appActivities.get(appActivities.size() - 1),
                    "id", getPackageName()));
            handler.removeView(view);
            handler.addView(viewNew);
            appActivities.remove(appActivities.get(appActivities.size() - 1));
            if (appActivities.get(appActivities.size() - 1).equals("home")) {
                buttonsActions.seekBar();
            }
        } else {
            Toast.makeText(getApplicationContext(), "Can't go back right here.",
                    Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public boolean onKeyLongPress(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            this.finish();
            System.exit(0);
            return true;
        }
        return super.onKeyLongPress(keyCode, event);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_main, menu);
        return super.onCreateOptionsMenu(menu);
    }


    protected void setupActionBar() {
        ActionBar actionBar = getActionBar();
        actionBar.setDisplayShowTitleEnabled(false);
        actionBar.setDisplayUseLogoEnabled(false);
        actionBar.setDisplayHomeAsUpEnabled(false);
        actionBar.setDisplayShowCustomEnabled(true);
        actionBar.setDisplayShowHomeEnabled(false);

        ActionBar.LayoutParams lp1 = new ActionBar.LayoutParams(ActionBar.LayoutParams.MATCH_PARENT, ActionBar.LayoutParams.MATCH_PARENT);
        View customNav = LayoutInflater.from(this).inflate(R.layout.action_bar, null, false); // layout which contains your button.

        actionBar.setCustomView(customNav, lp1);
    }

    /**
     * Drawers
     */

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // The action bar home/up action should open or close the drawer.
        // ActionBarDrawerToggle will take care of this.
        return mDrawerToggle.onOptionsItemSelected(item) || super.onOptionsItemSelected(item);
    }

    public void setDrawersLayouts() {
        inflater = LayoutInflater.from(this);
        ViewGroup viewLeft = (ViewGroup) inflater.inflate(R.layout.left_menu, mDrawerLeft, false);
        mDrawerLeft.addView(viewLeft);
        ViewGroup viewRight = (ViewGroup) inflater.inflate(R.layout.right_menu, mDrawerRight, false);
        mDrawerRight.addView(viewRight);
    }

    public static void closeDrawers() {
        mDrawerLayout.closeDrawer(mDrawerLeft);
        mDrawerLayout.closeDrawer(mDrawerRight);
    }

    public void reloadRightDrawer(String email) {
        HashMap<String, String> userMap = new DBTools(context).getUserInfo(email);
//        String personPhotoUrl = (String) userMap.get("avatar");
//        ImageView personPic = (ImageView) activity.findViewById(R.id.profilePic);
//        new LoadProfileImage(personPic).execute(personPhotoUrl);
        TextView firstName = (TextView) activity.findViewById(R.id.first_name);
        firstName.setText(userMap.get("firstName"));
        TextView surname = (TextView) activity.findViewById(R.id.surname);
        surname.setText(userMap.get("surname"));
        TextView lastName = (TextView) activity.findViewById(R.id.last_name);
        lastName.setText(userMap.get("lastName"));
    }

    public ArrayList<String> getAppActivities() {
        return appActivities;
    }

    public boolean setAppActivities(ArrayList<String> newAppAct) {
        appActivities = newAppAct;
        return true;
    }

    public int calledTime = 0;

    @Override
    protected void onActivityResult(int requestCode, int responseCode,
                                    Intent intent) {
        if (requestCode == 3) {
            if (responseCode == RESULT_OK) {
                DBTools database = new DBTools(getApplicationContext());
                HashMap<String, String> userInfo = new HashMap<>();
                String[] names = intent.getStringExtra("names").split(" ");
                if(!database.checkUserExistance(intent.getStringExtra("email"))) {
                    if (names.length >= 3) {
                        userInfo.put("firstName", names[0]);
                        userInfo.put("surname", names[1]);
                        userInfo.put("lastName", names[2]);
                    }else if(names.length == 2){
                        userInfo.put("firstName", names[0]);
                        userInfo.put("surname", names[1]);
                        userInfo.put("lastName", "");
                    }else if(names.length == 1){
                        userInfo.put("firstName", names[0]);
                        userInfo.put("surname", "");
                        userInfo.put("lastName", "");
                    }else if(names.length < 1){
                        userInfo.put("firstName", "");
                        userInfo.put("surname", "");
                        userInfo.put("lastName", "");
                    }
                    userInfo.put("avatar", intent.getStringExtra("avatar"));
                    userInfo.put("phoneNumber", "");
                    userInfo.put("emailAddress", intent.getStringExtra("email"));
                    userInfo.put("homeAddress", "");
                    userInfo.put("facebook", "");
                    userInfo.put("twitter", "");
                    userInfo.put("gPlus", "");
                    database.insertUser(userInfo);
                }
                updateUI(true, intent.getStringExtra("email"));
            } else {
                Toast.makeText(context, "Check your internet connection.", Toast.LENGTH_SHORT).show();
            }

        }else if(requestCode == 2){
            final Handler handler = new Handler();
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    // Do something after 5s = 5000ms
                    if(calledTime == 0){
                        Toast.makeText(getApplicationContext(), "Constellation found - Capricorn. Location - Bulgaria.", Toast.LENGTH_SHORT).show();
                        calledTime++;
                    }else if(calledTime == 1){
                        Toast.makeText(getApplicationContext(), "Constellation found - Triangulum Australe. Location - Bulgaria.", Toast.LENGTH_SHORT).show();
                        calledTime++;
                    }else {
                        Toast.makeText(getApplicationContext(), "Constellation found. Location - Bulgaria.", Toast.LENGTH_SHORT).show();
                    }
                }
            }, 2500);
        }
    }


    /**
     * Background Async task to load user profile picture from url
     */
    private class LoadProfileImage extends AsyncTask<String, Void, Bitmap> {
        ImageView bmImage;

        public LoadProfileImage(ImageView bmImage) {
            this.bmImage = bmImage;
        }

        protected Bitmap doInBackground(String... urls) {
            String urldisplay = urls[0];
            Bitmap mIcon11 = null;
            try {
                InputStream in = new java.net.URL(urldisplay).openStream();
                mIcon11 = BitmapFactory.decodeStream(in);
            } catch (Exception e) {
                Log.e("Error", e.getMessage());
                e.printStackTrace();
            }
            return mIcon11;
        }

        protected void onPostExecute(Bitmap result) {
            bmImage.setImageBitmap(result);
        }
    }

    private void updateUI(Boolean state, String email){
        if(state){
            HashMap<String, String> userInfo = new DBTools(context).getUserInfo(email);
            String personPhotoUrl = userInfo.get("avatar");
            ImageView personPic = (ImageView) activity.findViewById(R.id.profilePic);
            new LoadProfileImage(personPic).execute(personPhotoUrl);
            TextView firstName = (TextView) activity.findViewById(R.id.first_name);
            firstName.setText(userInfo.get("firstName"));
            TextView surname = (TextView) activity.findViewById(R.id.surname);
            surname.setText(userInfo.get("surname"));
            TextView lastName = (TextView) activity.findViewById(R.id.last_name);
            lastName.setText("");
            TextView userEmail = (TextView) activity.findViewById(R.id.txtEmail);
            userEmail.setText(email);

            /**
             * Set visibility
             */

            findViewById(R.id.btn_sign_in).setVisibility(View.GONE);
            findViewById(R.id.right_drawer_top_sub_sub).setVisibility(View.VISIBLE);
            findViewById(R.id.personalInf).setVisibility(View.VISIBLE);
            findViewById(R.id.right_drawer_bottom).setVisibility(View.VISIBLE);
            findViewById(R.id.editprftopbottom).setVisibility(View.VISIBLE);
            findViewById(R.id.editprfbottombottom).setVisibility(View.VISIBLE);
            findViewById(R.id.editprftopbottom).setVisibility(View.VISIBLE);
            findViewById(R.id.userEditBt).setVisibility(View.VISIBLE);
        }else {
            SignInButton button = (SignInButton) findViewById(R.id.btn_sign_in);
            button.setSize(SignInButton.SIZE_WIDE);
            findViewById(R.id.right_drawer_top_sub_sub).setVisibility(View.GONE);
            findViewById(R.id.personalInf).setVisibility(View.GONE);
            findViewById(R.id.right_drawer_bottom).setVisibility(View.GONE);
            findViewById(R.id.editprftopbottom).setVisibility(View.GONE);
            findViewById(R.id.editprfbottombottom).setVisibility(View.GONE);
            findViewById(R.id.editprftopbottom).setVisibility(View.GONE);
            findViewById(R.id.userEditBt).setVisibility(View.GONE);
        }
    }
}