package eu.techr.skykey.skykey;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.HashMap;

/**
 * Created by root on 15-3-16.
 */
public class DBTools extends SQLiteOpenHelper {
    // Context : provides access to application-specific resources and classes
    Context context;
    public DBTools(Context appContext) {
        // Call use the database or to create it
        super(appContext, "skykey.db", null, 1);
        context = appContext;

    }

    // onCreate is called the first time the database is created

    public void onCreate(SQLiteDatabase database) {

        // How to create a table in SQLite
        // Make sure you don't put a ; at the end of the query

        String query = "CREATE TABLE IF NOT EXISTS  user ( userId INTEGER PRIMARY KEY, firstName TEXT, " +
                "surname Text, lastName TEXT, avatar TEXT, phoneNumber TEXT, emailAddress TEXT, homeAddress TEXT, facebook TEXT, twitter TEXT, gPlus Text)";

        // Executes the query provided as long as the query isn't a select
        // or if the query doesn't return any data

        database.execSQL(query);

    }

    // onUpgrade is used to drop tables, add tables, or do anything
    // else it needs to upgrade
    // This is droping the table to delete the data and then calling
    // onCreate to make an empty table

    public void onUpgrade(SQLiteDatabase database, int version_old, int current_version) {
        onCreate(database);
    }

    public boolean checkUserExistance(String email){
        SQLiteDatabase database = this.getWritableDatabase();
        String count = "SELECT count(*) FROM user";
        Cursor mcursor = database.rawQuery(count, null);
        mcursor.moveToFirst();
        int icount = mcursor.getInt(0);
        if(icount > 0){
            return true;
        }else {
            return false;
        }
    }

    public void insertUser(HashMap<String, String> queryValues) {

        // Open a database for reading and writing

        SQLiteDatabase database = this.getWritableDatabase();

        // Stores key value pairs being the column name and the data
        // ContentValues data type is needed because the database
        // requires its data type to be passed

        ContentValues values = new ContentValues();

        values.put("firstName", queryValues.get("firstName"));
        values.put("surname", queryValues.get("surname"));
        values.put("lastName", queryValues.get("lastName"));
        values.put("avatar", queryValues.get("avatar"));
        values.put("phoneNumber", queryValues.get("phoneNumber"));
        values.put("emailAddress", queryValues.get("emailAddress"));
        values.put("homeAddress", queryValues.get("homeAddress"));
        values.put("facebook", queryValues.get("facebook"));
        values.put("twitter", queryValues.get("twitter"));
        values.put("gPlus", queryValues.get("gPlus"));

        // Inserts the data in the form of ContentValues into the
        // table name provided

        database.insert("user", null, values);

        // Release the reference to the SQLiteDatabase object

        database.close();
    }

    public int updateUser(HashMap<String, String> queryValues) {

        // Open a database for reading and writing

        SQLiteDatabase database = this.getWritableDatabase();

        // Stores key value pairs being the column name and the data

        ContentValues values = new ContentValues();

        values.put("firstName", queryValues.get("firstName"));
        values.put("surname", queryValues.get("surname"));
        values.put("lastName", queryValues.get("lastName"));
        values.put("avatar", queryValues.get("avatar"));
        values.put("phoneNumber", queryValues.get("phoneNumber"));
        values.put("emailAddress", queryValues.get("emailAddress"));
        values.put("homeAddress", queryValues.get("homeAddress"));
        values.put("facebook", queryValues.get("facebook"));
        values.put("twitter", queryValues.get("twitter"));
        values.put("gPlus", queryValues.get("gPlus"));

        // update(TableName, ContentValueForTable, WhereClause, ArgumentForWhereClause)

        return database.update("user", values, "userId" + " = ?", new String[] { queryValues.get("userId") });
    }

    public HashMap<String, String> getUserInfo(String email) {
        HashMap<String, String> userMap = new HashMap<>();

        // Open a database for reading only

        SQLiteDatabase database = this.getReadableDatabase();

        String selectQuery = "SELECT * FROM user where emailAddress='"+email+"'";

        // rawQuery executes the query and returns the result as a Cursor

        Cursor cursor = database.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()) {
            do {
                userMap.put("userId", cursor.getString(0));
                userMap.put("firstName", cursor.getString(1));
                userMap.put("surname", cursor.getString(2));
                userMap.put("lastName", cursor.getString(3));
                userMap.put("avatar", cursor.getString(4));
                userMap.put("phoneNumber", cursor.getString(5));
                userMap.put("emailAddress", cursor.getString(6));
                userMap.put("homeAddress", cursor.getString(7));
                userMap.put("facebook", cursor.getString(8));
                userMap.put("twitter", cursor.getString(9));
                userMap.put("gPlus", cursor.getString(10));

            } while (cursor.moveToNext());
        }
        cursor.close();
        return userMap;
    }
}
