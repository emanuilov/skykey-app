package eu.techr.skykey.skykey;

import android.animation.ValueAnimator;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.graphics.Bitmap;
import android.graphics.Matrix;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.text.SpannableString;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.ScaleGestureDetector;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.TranslateAnimation;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

/**
 * Created by root on 15-2-27.
 */
public class SetButtonsActions extends Activity {
    private Context context;
    private Activity activity;
    private LayoutInflater inflater;
    private ArrayList<String> activities;
    private View popupView;
    private PopupWindow popupWindow;
    private Integer isOnlinePressed = 0;
    private ImageView img;
    private Matrix matrix=new Matrix();
    private float scale=1f;
    private ScaleGestureDetector SGD;

    public SetButtonsActions(Context appContext, Activity appActivity, LayoutInflater appInflater) {
        context = appContext;
        activity = appActivity;
        inflater = appInflater;
        activities = new MainActivity().getAppActivities();
    }

    public void setOnClickListeners(String items[]) {
        // Get how many items will be checked
        int size = items.length;
        for (int count = 0; count < size; count++) {
            //Get the item
            if (!items[0].equals("settings")) {
                ViewGroup layoutHolder = (ViewGroup) activity.findViewById(context.getResources().getIdentifier(items[count],
                        "id", context.getPackageName()));
                int childcount = layoutHolder.getChildCount();
                //Check all of the childs of the item
                for (int i = 0; i < childcount; i++) {
                    View viewMain = layoutHolder.getChildAt(i);
                    if (viewMain.getId() != -1) {
                        String[] action = viewMain.getTag().toString().split("_");
                        if (action[0].equals("anim") && !viewMain.hasOnClickListeners()) {
                            viewMain.setOnClickListener(handlerAnim);
                        } else if (action[0].equals("menu")) {
                            ViewGroup viewInHolder = (ViewGroup) activity.findViewById(viewMain.getId());
                            int innerchildcount = viewInHolder.getChildCount();
                            for (int x = 0; x < innerchildcount; x++) {
                                View viewInnerHolder = viewInHolder.getChildAt(x);
                                if (viewInnerHolder.getId() != -1) {
                                    String[] string_in = viewInnerHolder.getTag().toString().split("_");
                                    if (string_in[0].equals("menu")) {
                                        ViewGroup viewInInnerHolder = (ViewGroup) activity.findViewById(viewInnerHolder.getId());
                                        int innerchildcount_in = viewInInnerHolder.getChildCount();
                                        for (int y = 0; y < innerchildcount_in; y++) {
                                            ViewGroup viewInInnerInHolder = (ViewGroup) activity.findViewById(viewInInnerHolder.getId());
                                            View viewInInnerInInnerHolder = viewInInnerInHolder.getChildAt(y);
                                            if (viewInInnerInInnerHolder.getId() != -1) {
                                                String[] string_ch_in_in = viewInInnerInInnerHolder.getTag().toString().split("_");
                                                if (string_ch_in_in[0].equals("menu")) {
                                                    ViewGroup viewInInnerInInnerInHolder = (ViewGroup) activity.findViewById(viewInInnerInInnerHolder.getId());
                                                    int innerchildcount_in_in = viewInInnerInInnerInHolder.getChildCount();
                                                    for (int z = 0; z < innerchildcount_in_in; z++) {
                                                        View viewInInnerInInnerItem = viewInInnerInInnerInHolder.getChildAt(z);
                                                        if (viewInInnerInInnerItem.getId() != -1) {
                                                            viewInInnerInInnerItem.setOnClickListener(handlerInf);
                                                        }
                                                    }
                                                } else if (!viewInInnerInInnerHolder.hasOnClickListeners()) {
                                                    viewInInnerInInnerHolder.setOnClickListener(handlerInf);
                                                }
                                            }
                                        }
                                    } else if (!viewInnerHolder.hasOnClickListeners()) {
                                        viewInnerHolder.setOnClickListener(handlerInf);
                                    }
                                }
                            }
                        }
                    }
                }
            } else {
                ViewGroup layoutHolder = (ViewGroup) inflater.inflate(context.getResources().getIdentifier(items[0],
                        "layout", context.getPackageName()), (ViewGroup) activity.findViewById(context.getResources().getIdentifier(items[0],
                        "id", context.getPackageName())), false);
                int childcount = layoutHolder.getChildCount();
                //Check all of the childs of the item
                for (int i = 0; i < childcount; i++) {
                    View viewMain = layoutHolder.getChildAt(i);
                    if (viewMain.getId() != -1) {
                        String[] action = viewMain.getTag().toString().split("_");
                        if (action[0].equals("menu")) {
                            ViewGroup viewIn = (ViewGroup) layoutHolder.findViewById(viewMain.getId());
                            int childcount_in = viewIn.getChildCount();
                            for (int z = 0; z < childcount_in; z++) {
                                View viewInItem = viewIn.getChildAt(z);
                                if (viewInItem.getId() != -1) {
                                    String[] action_in = viewInItem.getTag().toString().split("_");
                                    if (action_in[0].equals("menu")) {
                                        ViewGroup viewInner = (ViewGroup) layoutHolder.findViewById(viewInItem.getId());
                                        int childcount_inner = viewInner.getChildCount();
                                        for (int x = 0; x < childcount_inner; x++) {
                                            View viewInnerItem = viewInner.getChildAt(x);
                                            if (viewInnerItem.getId() != -1) {
                                                popupView.findViewById(viewInnerItem.getId()).setOnClickListener(handlerInf);
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    public void setPopUpOnClickListeners(View holder, String[] items){
        ViewGroup layoutHolder = (ViewGroup) inflater.inflate(context.getResources().getIdentifier(items[0],
                "layout", context.getPackageName()), (ViewGroup) activity.findViewById(context.getResources().getIdentifier(items[0],
                "id", context.getPackageName())), false);
        int childs = layoutHolder.getChildCount();
        for(int i = 0;i < childs; i++){
            View child = layoutHolder.getChildAt(i);
            if(child.getId() != -1){
                holder.findViewById(child.getId()).setOnClickListener(handlerInf);
            }
        }
    }

    View.OnClickListener handlerInf = new View.OnClickListener() {
        public void onClick(View v) {
            String[] tag = v.getTag().toString().split("_");
            if (!tag[0].equals("action")) {
                activity.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_UNSPECIFIED);
                activities.add(v.getTag().toString());
                LinearLayout handler = (LinearLayout) activity.findViewById(R.id.content_frame);
                ViewGroup viewNew = (ViewGroup) inflater.inflate(context.getResources().getIdentifier(activities.get(activities.size() - 1),
                        "layout", context.getPackageName()), handler, false);
                ViewGroup view = (ViewGroup) activity.findViewById(context.getResources().getIdentifier(activities.get(activities.size() - 2),
                        "id", context.getPackageName()));
                handler.removeView(view);
                handler.addView(viewNew);
                if(tag[0].equals("news")){
                    String text = activity.getString(R.string.news_post);

                    SpannableString ss = new SpannableString(text);
                    ss.setSpan(new WrapText(3, 0), 0, ss.length(), 0);

                    TextView messageView = (TextView) activity.findViewById(R.id.postText);
                    messageView.setText(ss);
                }
                String[] items = {activities.get(activities.size() - 1), "action_barLayout"};
                setOnClickListeners(items);
                if (items[0].equals("home")) {
                    seekBar();
                } else {
                    ImageView logo = (ImageView) activity.findViewById(R.id.action_bar_logo);
                    logo.setVisibility(View.VISIBLE);
                }
                if(popupWindow != null){
                    popupWindow.dismiss();
                }
                if(tag[0].equals("sun")){
                    img = (ImageView) activity.findViewById(R.id.sun_system_image);
                    SGD = new ScaleGestureDetector(activity,new ScaleListener());
                }
                if(tag.length == 1 && tag[0].equals("map")){
                    new ConstellationMap(activity);
                }
                MainActivity.closeDrawers();
            } else switch (tag[1]) {
                case "locate":
                    Intent findConst = new Intent(activity, FindConstellation.class);
                    activity.startActivityForResult(findConst, 2);
                    MainActivity.closeDrawers();
                    break;
                case "settings":
                    settingsTop(v);
                    MainActivity.closeDrawers();
                    break;
                case "url":
                    if(tag[2].equals("fb")){
                        Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://www.facebook.com/TechR.euTech"));
                        activity.startActivity(browserIntent);
                    } else if(tag[2].equals("gp")){
                        Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://plus.google.com/b/106171344372927211525/+TechrEu_eu/about"));
                        activity.startActivity(browserIntent);
                    } else if(tag[2].equals("tw")){
                        Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://twitter.com/TechR_eu"));
                        activity.startActivity(browserIntent);
                    }
                    MainActivity.closeDrawers();
                    break;
                case "signIn":
                    Intent loginG = new Intent(activity, GooglePlusSingIn.class);
                    //TODO check does this below works
                    loginG.putExtra("button", "1");
                    activity.startActivityForResult(loginG, 3);
                    break;
                case "online":
                    if(isOnlinePressed == 0) {
                        Toast.makeText(context, "Online", Toast.LENGTH_SHORT).show();
                        isOnlinePressed = 1;
                    } else {
                        Toast.makeText(context, "Offline", Toast.LENGTH_SHORT).show();
                        isOnlinePressed = 0;
                    }
                    MainActivity.closeDrawers();
                    break;
                case "search":
                    SearchBox searchBox = new SearchBox(activity);
                    if(tag[2].equals("box")){
                        searchBox.searchInContent();
                    }
                    break;
                case "solar":
                    Intent solarSystem = new Intent(activity, LoadSolarSystem.class);
                    activity.startActivity(solarSystem);
                    break;
            }
            new MainActivity().setAppActivities(activities);

        }
    };

    @Override
    public boolean onTouchEvent(MotionEvent ev){
        SGD.onTouchEvent(ev);
        return true;
    }

    private class ScaleListener extends ScaleGestureDetector.
            SimpleOnScaleGestureListener {
        @Override
        public boolean onScale(ScaleGestureDetector detector) {
            scale *= detector.getScaleFactor();
            scale = Math.max(0.1f, Math.min(scale, 5.0f));
            matrix.setScale(scale, scale);
            img.setImageMatrix(matrix);
            return true;
        }
    }

    View.OnClickListener handlerAnim = new View.OnClickListener() {
        public void onClick(View v) {
            toggle(v, 3);
        }
    };

    public void settingsTop(View v) {
        LinearLayout handler = new LinearLayout(activity);
        popupView = inflater.inflate(R.layout.settings, handler, false);
        popupWindow = new PopupWindow(
                popupView,
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);
        Bitmap bitmap = null;
        popupWindow.setBackgroundDrawable(new BitmapDrawable(context.getResources(), bitmap));
        popupWindow.setOutsideTouchable(true);
        popupWindow.showAsDropDown(v, 50, 11);
        String[] menu = {"settings"};
        setOnClickListeners(menu);
    }

    protected void seekBar() {
        ImageView logo = (ImageView) activity.findViewById(R.id.action_bar_logo);
        logo.setVisibility(View.GONE);
        SeekBar sb = (SeekBar) activity.findViewById(R.id.slide_to_go);
        DisplayMetrics dm = context.getResources().getDisplayMetrics();
        int width = dm.widthPixels;
        int height = dm.heightPixels;
        int dens = dm.densityDpi;
        double wi = (double) width / (double) dens;
        double hi = (double) height / (double) dens;
        double x = Math.pow(wi, 2);
        double y = Math.pow(hi, 2);
        double screenInches = Math.sqrt(x + y);
        RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams) sb.getLayoutParams();
        if (screenInches < 6.8) {
            activity.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
            layoutParams.addRule(RelativeLayout.ALIGN_PARENT_RIGHT, RelativeLayout.TRUE);
            sb.setLayoutParams(layoutParams);
        } else {
            layoutParams.addRule(RelativeLayout.CENTER_HORIZONTAL, RelativeLayout.TRUE);
            float scale = context.getResources().getDisplayMetrics().density;
            int dp = (int) (73 * scale + 0.5f);
            sb.setPadding(dp, 0, 0, 0);
            sb.setLayoutParams(layoutParams);
        }
        sb.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            public void onStopTrackingTouch(final SeekBar seekBar) {
                ValueAnimator anim = ValueAnimator.ofInt(seekBar.getProgress(), 0);
                anim.setDuration(100);
                anim.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
                    @Override
                    public void onAnimationUpdate(ValueAnimator animation) {
                        int animProgress = (Integer) animation.getAnimatedValue();
                        seekBar.setProgress(animProgress);
                    }
                });
                anim.start();
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onProgressChanged(SeekBar seekBar, int progress,
                                          boolean fromUser) {
                if (progress > 90) {
                    LinearLayout handler = (LinearLayout) activity.findViewById(R.id.content_frame);
                    RelativeLayout home = (RelativeLayout) activity.findViewById(R.id.home);
                    ViewGroup viewNew = (ViewGroup) inflater.inflate(R.layout.locate_const, handler, false);
                    handler.removeView(home);
                    handler.addView(viewNew);
                    activities.add("locate_const");
                    String[] items = {"locate_const"};
                    setOnClickListeners(items);
                    ImageView logo = (ImageView) activity.findViewById(R.id.action_bar_logo);
                    logo.setVisibility(View.VISIBLE);
                    new MainActivity().setAppActivities(activities);
                }

            }
        });
    }

    /**
     * Toggle Animation
     * item is after how views in the is the item which we want to hide
     */
    View view = null;

    public void toggle(View v, int item) {
        TranslateAnimation anim;
        ViewGroup parent = (ViewGroup) v.getParent();
        int childcount = parent.getChildCount();
        for (int itemPos = 0; itemPos < childcount; itemPos++) {
            view = parent.getChildAt(itemPos);
            if (view.getId() == v.getId()) {
                view = parent.getChildAt(itemPos + item);
                break;
            }
        }
        boolean isOpen = view.isShown();
        int height = view.getHeight() * (-1);
        if (!isOpen) {
            view.setVisibility(View.VISIBLE);
            anim = new TranslateAnimation(0.0f, 0.0f, height, 0.0f);
        } else {
            anim = new TranslateAnimation(0.0f, 0.0f, 0.0f, height);
            anim.setAnimationListener(collapseListener);
        }

        anim.setDuration(300);
        anim.setFillEnabled(false);
        view.startAnimation(anim);
    }

    Animation.AnimationListener collapseListener = new Animation.AnimationListener() {
        public void onAnimationEnd(Animation animation) {
            view.setVisibility(View.GONE);
        }

        @Override
        public void onAnimationRepeat(Animation animation) {
        }

        @Override
        public void onAnimationStart(Animation animation) {
        }
    };
}
