package eu.techr.skykey.skykey;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Log;
import android.widget.Toast;

import org.opencv.android.OpenCVLoader;
import org.opencv.core.Mat;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import static org.opencv.highgui.Highgui.imread;

/**
 * Created by root on 15-2-18.
 */

public class FindConstellation extends Activity{
    /**
     * OpenCV Vars
     */

    /**
     * Take Photo Vars
     */
    public String mCurrentPhotoPath;
    static final int REQUEST_TAKE_PHOTO = 2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (!OpenCVLoader.initDebug()) {
            // Handle initialization error
        }
        dispatchTakePictureIntent();
    }

    /**
     * Find Const
     */
    /** Load the native library where the native method
     * is stored.
     */
    static {
        System.loadLibrary("jni_part");
    }

    public native String findConst(String img);

    private long loadImage(String location){
        Mat img = imread(location);
        return img.getNativeObjAddr();
    }


    /**
     * Take Photo
     */

    private void dispatchTakePictureIntent() {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        // Ensure that there's a camera activity to handle the intent
        if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
            // Create the File where the photo should go
            File photoFile = null;
            try {
                photoFile = createImageFile();
            } catch (IOException ex) {
                // Error occurred while creating the File

            }
            // Continue only if the File was successfully created
            if (photoFile != null) {
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT,
                        Uri.fromFile(photoFile));
                startActivityForResult(takePictureIntent, REQUEST_TAKE_PHOTO);
            }
        }
    }

    int timeCalled = 0;

    protected void onActivityResult(int requestCode, int resultCode, Intent takePictureIntent) {
        if (requestCode == REQUEST_TAKE_PHOTO) {
            if(resultCode == RESULT_OK){
//                int result = Integer.parseInt(findConst(mCurrentPhotoPath));
//                if(result != 0) {
//                    if (result > 4) {
//                        Toast.makeText(getApplicationContext(), "Constellation found. Location Bulgaria", Toast.LENGTH_SHORT).show();
//                    } else {
//                        Toast.makeText(getApplicationContext(), "Constellation not found.", Toast.LENGTH_SHORT).show();
//                    }
//                } else {
//                    Toast.makeText(getApplicationContext(), "Cannot load the image.", Toast.LENGTH_SHORT).show();
//                }
//                loadImage(mCurrentPhotoPath);
//                int result = Integer.parseInt(findConst(loadImage(mCurrentPhotoPath)));
//                if(result > 4){
//                    Toast.makeText(getApplicationContext(), "Constellation found. Location Bulgaria", Toast.LENGTH_SHORT).show();
//                }
//                if(result > 2) {
//                final Handler handler = new Handler();
//                handler.postDelayed(new Runnable() {
//                    @Override
//                    public void run() {
//                        // Do something after 5s = 5000ms
//                        if(timeCalled == 0){
//                            Toast.makeText(getApplicationContext(), "Constellation found - Capricorn. Location - Bulgaria.", Toast.LENGTH_SHORT).show();
//                            timeCalled++;
//                        }else if(timeCalled == 1){
//                            Toast.makeText(getApplicationContext(), "Constellation found - Triangulum Australe. Location - Bulgaria.", Toast.LENGTH_SHORT).show();
//                            timeCalled++;
//                        }else {
//                            Toast.makeText(getApplicationContext(), "Constellation found. Location - Bulgaria.", Toast.LENGTH_SHORT).show();
//                        }
//                    }
//                }, 2500);
//                } else {
//                    Toast.makeText(getApplicationContext(), "Constellation not found.", Toast.LENGTH_SHORT).show();
//                }
                Intent returnIntent = new Intent();
                returnIntent.putExtra("path",mCurrentPhotoPath);
                setResult(RESULT_OK,returnIntent);
                finish();
            } else{
                File file = new File(mCurrentPhotoPath);
                file.delete();
                Intent returnIntent = new Intent();
                setResult(RESULT_CANCELED,returnIntent);
                finish();
            }
        }
    }

    private File createImageFile() throws IOException {
        File file = new File(Environment.getExternalStorageDirectory(), Environment.DIRECTORY_PICTURES + "/SkyKey");
        if (!file.exists()) {
            if (!file.mkdirs()) {
            }
        }
        // Create an image file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "PNG_" + timeStamp + "_";
        String dir = Environment.DIRECTORY_PICTURES + "/SkyKey";
        File storageDir = Environment.getExternalStoragePublicDirectory(
                dir);
        File image = File.createTempFile(
                imageFileName,  /* prefix */
                ".png",         /* suffix */
                storageDir      /* directory */
        );

        // Save a file: path for use with ACTION_VIEW intents
        mCurrentPhotoPath = image.getAbsolutePath();
        return image;
    }

}
